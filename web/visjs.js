//https://css-tricks.com/snippets/javascript/lighten-darken-color/
function LightenDarkenColor(col, amt) {

    var usePound = false;

    if (col[0] == "#") {
        col = col.slice(1);
        usePound = true;
    }

    var num = parseInt(col, 16);

    var r = (num >> 16) + amt;

    if (r > 255) r = 255;
    else if (r < 0) r = 0;

    var b = ((num >> 8) & 0x00FF) + amt;

    if (b > 255) b = 255;
    else if (b < 0) b = 0;

    var g = (num & 0x0000FF) + amt;

    if (g > 255) g = 255;
    else if (g < 0) g = 0;

    return (usePound ? "#" : "") + (g | (b << 8) | (r << 16)).toString(16);

}

// create the svg for the ring
const svgEl = document.querySelector('svg');
const slices = [
    {percent: 0.1, color: '#FED00D'},
    {percent: 0.1, color: '#F37220'},
    {percent: 0.1, color: '#ED0617'},
    {percent: 0.1, color: '#D81681'},
    {percent: 0.1, color: '#782489'},
    {percent: 0.1, color: '#1D4EBC'},
    {percent: 0.1, color: '#139FDA'},
    {percent: 0.1, color: '#49BDA2'},
    {percent: 0.1, color: '#077C4A'},
    {percent: 0.1, color: '#72BF43'},

];
let cumulativePercent = 0;

function getCoordinatesForPercent(percent) {
    const x = Math.cos(2 * Math.PI * percent);
    const y = Math.sin(2 * Math.PI * percent);
    return [x, y];
}

function getCoordinatesForInner(percent) {
    const x = (Math.cos(2 * Math.PI * percent)) * 0.7;
    const y = (Math.sin(2 * Math.PI * percent)) * 0.7;
    return [x, y];
}

for (let i = 0; i < slices.length; i++) {
    // destructuring assignment sets the two variables at once
    const [startX, startY] = getCoordinatesForPercent(cumulativePercent);
    const [startInnerX, startInnerY] = getCoordinatesForInner(cumulativePercent);

// each slice starts where the last slice ended, so keep a cumulative percent
    cumulativePercent += slices[i].percent;

    const [endX, endY] = getCoordinatesForPercent(cumulativePercent);

    const [endInnerX, endInnerY] = getCoordinatesForInner(cumulativePercent);

// if the slice is more than 50%, take the large arc (the long way around)
    const largeArcFlag = slices[i].percent > .5 ? 1 : 0;

// create an array and join it just for code readability
    const pathData = [
        `M ${startX} ${startY}`, // Move
        `A 1 1 0 ${largeArcFlag} 1 ${endX} ${endY}`, // Arc
        `L ${endInnerX} ${endInnerY}`, // Line
        //changing the 'sweep flag' from 1 to 0 fixed the lumps on the pie
        `A 0.7 0.7 0 ${largeArcFlag} 0 ${startInnerX} ${startInnerY}`, // Arc
    ].join(' ');

// create a <path> and append it to the <svg> element
    const pathEl = document.createElementNS('http://www.w3.org/2000/svg', 'path');
    pathEl.setAttribute('d', pathData);
    pathEl.setAttribute('fill', slices[i].color);
    pathEl.setAttribute('id', 'slice' + i);
    pathEl.style.zIndex = '20';


    var numcolours = 10;

    pathEl.onmouseover = function () {
        this.setAttribute('stroke', slices[i].color);
        this.setAttribute('stroke-width', 0.05);
        this.setAttribute('stroke-linecap', 'butt')
    };
    pathEl.onmouseout = function () {
        this.setAttribute('stroke-width', 0);
    };

    pathEl.onclick = function () {
        for (let j = 0; j < slices.length; j++) {
            var slice = document.getElementById('slice' + j);
            slice.setAttribute('fill', slices[j].color);
            slice.setAttribute('fill', LightenDarkenColor(slice.getAttribute('fill'), 120));
            //console.log(slice.id);
        }

        if (this.getAttribute('fill') === slices[i].color) {
            for (let j = 0; j < slices.length; j++) {
                var slice = document.getElementById('slice' + j);
                slice.setAttribute('fill', LightenDarkenColor(slice.getAttribute('fill'), 120));
                //console.log(slice.id);
            }
            this.setAttribute('fill', slices[i].color);
            //console.log(this.id);

        } else if (this.getAttribute('fill') === '#999999') {
            this.setAttribute('fill', slices[i].color);
            //console.log(this);

        }
    };


    //add interactions for each segment

    svgEl.appendChild(pathEl);


}

var curvetext = document.createElement('text');
var text = document.createElement('textPath');
text.setAttribute('href', document.getElementById('slice0'));
var words = document.createTextNode('team');
text.appendChild(words);
console.log('did a thing');
curvetext.appendChild(words);
svgEl.appendChild(curvetext);


//making the "all players" sidebar collapsible
// var coll = document.getElementsByClassName("collapsible");
// var i;
//
// for (i = 0; i < coll.length; i++) {
//     coll[i].addEventListener("click", function() {
//         this.classList.toggle("active");
//         var content = this.nextElementSibling;
//         if (content.style.display === "block") {
//             content.style.display = "none";
//         } else {
//             content.style.display = "block";
//         }
//     });
// }

/* Set the width of the sidebar to 250px and the left margin of the page content to 250px */
function openNav() {
    document.getElementById("mySidebar").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
}

/* Set the width of the sidebar to 0 and the left margin of the page content to 0 */
function closeNav() {
    document.getElementById("mySidebar").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
}


//list of names appears when pens slice is clicked
document.getElementById('slice0').onclick = function () {
    document.getElementById('cmatches').style.display = 'none';

    if (numcolours === 10) {
        var team1 = document.getElementById('team1');
        while (team1.firstChild) {
            team1.removeChild(team1.firstChild);
        }
        var header = document.createElement('h2');
        var teamname = document.createTextNode('Penguins');
        header.appendChild(teamname);
        team1.appendChild(header);
        var list = document.createElement('ul');
        list.setAttribute('id', 'penslist');

        var m1 = document.createElement('li');
        m1.appendChild(document.createTextNode('Armstrong, C'));
        list.appendChild(m1);
        var m1 = document.createElement('li');
        m1.appendChild(document.createTextNode('Crosby, S'));
        m1.addEventListener('click', crosby);
        list.appendChild(m1);
        var m1 = document.createElement('li');
        m1.appendChild(document.createTextNode('Cullen, M'));
        list.appendChild(m1);
        var m1 = document.createElement('li');
        m1.appendChild(document.createTextNode('Dumoulin, B'));
        list.appendChild(m1);
        var m1 = document.createElement('li');
        m1.appendChild(document.createTextNode('Eaves, P'));
        list.appendChild(m1);
        var m1 = document.createElement('li');
        m1.appendChild(document.createTextNode('Fleury, M-A'));
        list.appendChild(m1);
        var m1 = document.createElement('li');
        m1.appendChild(document.createTextNode('Guentzel, J'));
        list.appendChild(m1);
        var m1 = document.createElement('li');
        var image = document.createElement('img');
        image.setAttribute('src', 'images/both.png');
        m1.appendChild(image);
        m1.appendChild(document.createTextNode(' Hagelin, C'));
        list.appendChild(m1);
        var m1 = document.createElement('li');
        m1.appendChild(document.createTextNode('Hornqvist, P'));
        list.appendChild(m1);
        var m1 = document.createElement('li');
        m1.appendChild(document.createTextNode('Jagr, J'));
        list.appendChild(m1);
        var m1 = document.createElement('li');
        m1.appendChild(document.createTextNode('Kunitz, C'));
        list.appendChild(m1);
        var m1 = document.createElement('li');
        m1.appendChild(document.createTextNode('Lemieux, M'));
        list.appendChild(m1);
        var m1 = document.createElement('li');
        m1.appendChild(document.createTextNode('Malkin, E'));
        list.appendChild(m1);
        var m1 = document.createElement('li');
        m1.appendChild(document.createTextNode('Murray, M'));
        list.appendChild(m1);
        var m1 = document.createElement('li');
        var image = document.createElement('img');
        image.setAttribute('src', 'images/both.png');
        m1.appendChild(image);
        m1.appendChild(document.createTextNode(' Niskanen, M'));
        list.appendChild(m1);
        var m1 = document.createElement('li');
        var image = document.createElement('img');
        image.setAttribute('src', 'images/both.png');
        m1.appendChild(image);
        m1.appendChild(document.createTextNode(' Orpik, B'));
        list.appendChild(m1);
        var m1 = document.createElement('li');
        m1.appendChild(document.createTextNode('Rust, B'));
        list.appendChild(m1);

        var m1 = document.createElement('li');
        m1.appendChild(document.createTextNode('Sheary, C'));
        list.appendChild(m1);


        team1.appendChild(list);

        var penslinks = document.getElementById('penslinks');
        penslinks.style.display = 'block';


        for (let j = 0; j < slices.length; j++) {
            var slice = document.getElementById('slice' + j);
            slice.setAttribute('fill', slices[j].color);
            slice.setAttribute('fill', LightenDarkenColor(slice.getAttribute('fill'), 160));
            //console.log(slice.id);
        }
        this.setAttribute('fill', '#FED00D');
        //console.log(this.id);
        numcolours = 1;
        console.log(numcolours);

        var players = document.getElementById('allplayers');
        var pens = players.getElementsByClassName('pens');
        for (var i in pens) {
            if (pens.hasOwnProperty(i)) {

                console.log(pens[i]);
                var p = pens[i];
                p.style.display = 'none';
            }
        }


        console.log('but where is the limes??');

    }
    else {
        this.setAttribute('fill', '#FED00D');
        //console.log(this);

    }

};


document.getElementById('slice7').onclick = function () {
    console.log("numcols= " + numcolours);
    if (numcolours === 1 && document.getElementById('slice0').getAttribute('fill') === '#FED00D') {
        //alert('boom')
        this.setAttribute('fill', '#49BDA2');
        //console.log(this.id);
        numcolours = 2;

        var team1 = document.getElementById('team2');
        while (team1.firstChild) {
            team1.removeChild(team1.firstChild);
        }

        var header = document.createElement('h2');
        var teamname = document.createTextNode('Tigers');
        header.appendChild(teamname);
        team1.appendChild(header);
        var list = document.createElement('ul');
        list.setAttribute('id', 'capslist');

        var m1 = document.createElement('li');
        m1.appendChild(document.createTextNode('Backstrom, N'));
        list.appendChild(m1);
        var m1 = document.createElement('li');
        m1.appendChild(document.createTextNode('Copley, P'));
        list.appendChild(m1);
        var m1 = document.createElement('li');
        m1.appendChild(document.createTextNode('Carlson, J'));
        list.appendChild(m1);
        var m1 = document.createElement('li');
        m1.appendChild(document.createTextNode('Djoos'));
        list.appendChild(m1);
        var m1 = document.createElement('li');
        m1.appendChild(document.createTextNode('Eller, L'));
        list.appendChild(m1);
        var m1 = document.createElement('li');
        var image = document.createElement('img');
        image.setAttribute('src', 'images/both.png');
        m1.appendChild(image);
        m1.appendChild(document.createTextNode(' Hagelin, C'));
        list.appendChild(m1);
        var m1 = document.createElement('li');
        m1.appendChild(document.createTextNode('Holtby, B'));
        list.appendChild(m1);
        var m1 = document.createElement('li');
        m1.appendChild(document.createTextNode('Kuznetsov, E'));
        list.appendChild(m1);
        var m1 = document.createElement('li');
        m1.appendChild(document.createTextNode('Kempny, M'));
        list.appendChild(m1);
        var m1 = document.createElement('li');
        var image = document.createElement('img');
        image.setAttribute('src', 'images/both.png');
        m1.appendChild(image);
        m1.appendChild(document.createTextNode(' Niskanen, M'));
        list.appendChild(m1);
        var m1 = document.createElement('li');
        var image = document.createElement('img');
        image.setAttribute('src', 'images/both.png');
        m1.appendChild(image);
        m1.appendChild(document.createTextNode(' Orpik, B'));
        list.appendChild(m1);
        var m1 = document.createElement('li');
        m1.appendChild(document.createTextNode('Ovechkin, A'));
        list.appendChild(m1);
        var m1 = document.createElement('li');
        m1.appendChild(document.createTextNode('Oshie, T'));
        list.appendChild(m1);
        var m1 = document.createElement('li');
        m1.appendChild(document.createTextNode('Stephenson, C'));
        list.appendChild(m1);

        var m1 = document.createElement('li');
        m1.appendChild(document.createTextNode('Walker, N'));
        list.appendChild(m1);
        var m1 = document.createElement('li');
        m1.appendChild(document.createTextNode('Wilson, T'));
        list.appendChild(m1);


        team1.appendChild(list);

        closeNav();

        var pairlines = document.getElementById('pairteams');
        pairlines.style.display = 'block';

        var penslinks = document.getElementById('penslinks');
        penslinks.style.display = 'none';

        var players = document.getElementById('allplayers');
        var pens = players.getElementsByClassName('caps');
        for (var i in pens) {
            console.log(pens[i]);
            var item = pens[i];
            item.style.display = 'none';
        }

    }
};

document.getElementById('line1').onclick = function () {
    console.log(this);
};

document.getElementById('line2').onclick = function () {
    console.log('that');
    var other = document.getElementById('line1');
    other.style.display = 'none';


    var penslist = document.getElementById('penslist');
    console.log(penslist);
    while (penslist.firstChild) {
        penslist.removeChild(penslist.firstChild);
    }

    var m1 = document.createElement('li');
    m1.appendChild(document.createTextNode('Crosby, S'));
    m1.addEventListener('click', crosby);
    penslist.appendChild(m1);
    var m1 = document.createElement('li');
    m1.appendChild(document.createTextNode('Cullen, M'));
    penslist.appendChild(m1);
    var m1 = document.createElement('li');
    m1.appendChild(document.createTextNode('Dumoulin, B'));
    penslist.appendChild(m1);

    var m1 = document.createElement('li');
    m1.appendChild(document.createTextNode('Fleury, M-A'));
    penslist.appendChild(m1);
    var m1 = document.createElement('li');
    m1.appendChild(document.createTextNode('Guentzel, J'));
    penslist.appendChild(m1);
    var m1 = document.createElement('li');
    m1.appendChild(document.createTextNode('Hagelin, C'));
    penslist.appendChild(m1);
    var m1 = document.createElement('li');
    m1.appendChild(document.createTextNode('Hornqvist, P'));
    penslist.appendChild(m1);

    var m1 = document.createElement('li');
    m1.appendChild(document.createTextNode('Kunitz, C'));
    penslist.appendChild(m1);

    var m1 = document.createElement('li');
    m1.appendChild(document.createTextNode('Malkin, E'));
    penslist.appendChild(m1);
    var m1 = document.createElement('li');
    m1.appendChild(document.createTextNode('Murray, M'));
    penslist.appendChild(m1);

    var m1 = document.createElement('li');
    m1.appendChild(document.createTextNode('Rust, B'));
    penslist.appendChild(m1);

    var capslist = document.getElementById('capslist');
    console.log(capslist);
    while (capslist.firstChild) {
        capslist.removeChild(capslist.firstChild);
    }

    var m1 = document.createElement('li');
    m1.appendChild(document.createTextNode('Backstrom, N'));
    capslist.appendChild(m1);
    var m1 = document.createElement('li');
    m1.appendChild(document.createTextNode('Copley, P'));
    capslist.appendChild(m1);
    var m1 = document.createElement('li');
    m1.appendChild(document.createTextNode('Carlson, J'));
    capslist.appendChild(m1);
    var m1 = document.createElement('li');
    m1.appendChild(document.createTextNode('Eller, L'));
    capslist.appendChild(m1);
    var m1 = document.createElement('li');
    m1.appendChild(document.createTextNode('Holtby, B'));
    capslist.appendChild(m1);
    var m1 = document.createElement('li');
    m1.appendChild(document.createTextNode('Kempny, M'));
    capslist.appendChild(m1);
    var m1 = document.createElement('li');
    m1.appendChild(document.createTextNode('Niskanen, M'));
    capslist.appendChild(m1);
    var m1 = document.createElement('li');
    m1.appendChild(document.createTextNode('Orpik, B'));
    capslist.appendChild(m1);
    var m1 = document.createElement('li');
    m1.appendChild(document.createTextNode('Ovechkin, A'));
    capslist.appendChild(m1);
    var m1 = document.createElement('li');
    m1.appendChild(document.createTextNode('Oshie, T'));
    capslist.appendChild(m1);
    var m1 = document.createElement('li');
    m1.appendChild(document.createTextNode('Wilson, T'));
    capslist.appendChild(m1);

    document.getElementById("score").style.display = 'block';

};

// implemet interaction when player name is clicked
document.getElementById('crosby').onclick = crosby;

function crosby () {

    console.log("clicked crosby");
    for (let j = 0; j < slices.length; j++) {
        var slice = document.getElementById('slice' + j);
        slice.setAttribute('fill', slices[j].color);
        slice.setAttribute('fill', LightenDarkenColor(slice.getAttribute('fill'), 160));
    }
document.getElementById('penslinks').style.display = 'none';
    document.getElementById('pairteams').style.display = 'none';
    numcolours = 10;

    document.getElementById('slice0').setAttribute('fill', slices[0].color);
    document.getElementById('slice3').setAttribute('fill', slices[3].color);
    document.getElementById('slice5').setAttribute('fill', slices[5].color);

    document.getElementById('cmatches').style.display = "block";

    var div = document.getElementById('team1');
    while (div.firstChild) {
        div.removeChild(div.firstChild);
    }
    var otherteam = document.getElementById('team2');
    while (otherteam.firstChild){
        otherteam.removeChild(otherteam.firstChild);
    }
    var header = document.createElement('h2');
    var name = document.createTextNode('Crosby, Sidney');
    header.appendChild(name);
    div.appendChild(header);
    var team1 = document.createElement('p');
    team1.setAttribute('class', 'team');
    var text1 = document.createTextNode('Penguins 12 - 7 Sharks');
    team1.appendChild(text1);
    div.appendChild(team1);
    var team2 = document.createElement('p');
    team2.setAttribute('class', 'team');
    var text2 = document.createTextNode('Lions 5 - 8 Sealions');
    team2.appendChild(text2);
    div.appendChild(team2);
    var team3 = document.createElement('p');
    team3.setAttribute('class', 'team');
    var text3 = document.createTextNode('Kiwis 10 - 3 Lemons');
    team3.appendChild(text3);
    div.appendChild(team3);

}

function restart() {
    location.reload();
}



